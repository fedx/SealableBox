# Blender Parametric Sealable Box.

## How to use the paramaters:
Similar to OpenSCAD, I have variables defined at the top of the file, I hope they make sense, but if you have any questions please feel free to open a bug report.0

## Roadmap:

- [x] The base box design.
- [ ] Latch and hindge implimentation
- [ ] Finish the side-latches (currently they are broken, but it won't be hard to finish)
- [x] Seal
- [ ] Future features...



import bpy
import bmesh

##############
# Parameters #
##############

length = 100
width = 50
lower_height = 12
upper_height = 12
shell_width = 4

bevel_radius = 4

latch_num = 2

side_latches = 0
#############################################
########################################################################
bpy.ops.object.select_all(action='SELECT')                           
lower_height-=4
upper_height-=4
bpy.ops.object.delete(use_global=False, confirm=False)
#################################################################
def create_box(top, lenght, width, height, shell_width):
    latch_width = 30
    latch_depth = 5
    latch_spacing = 5
    latch_inner_width = 20
    old_shell_width = shell_width

    
    for i in range(-1*(latch_num-1), latch_num, 2):
        for j in range(-1, 2, 2):
        
            bpy.ops.mesh.primitive_cube_add(size=1, 
                enter_editmode=False, 
                align='WORLD', 
                location=(i*length/(2*latch_num), j*(width/2+shell_width+1/3*shell_width+latch_depth/2), 0),
                scale=(latch_width-(latch_width-latch_inner_width), latch_depth+shell_width*4, lower_height+shell_width*2))
            bpy.context.object.name = "latch_cutout"
            bpy.ops.mesh.primitive_cube_add(size=1, 
                enter_editmode=False, 
                align='WORLD', 
                location=(i*length/(2*latch_num), j*(width/2+shell_width-bevel_radius/2), height/-2-shell_width/2),
                scale=(latch_width, bevel_radius, bevel_radius))
            bpy.context.object.name = "latch_attachment"
            bpy.ops.mesh.primitive_cube_add(size=1, 
                enter_editmode=False, 
                align='WORLD', 
                location=(i*length/(2*latch_num), j*(width/2+shell_width+1/3*shell_width+latch_depth/2), 0),
                scale=(latch_width, latch_depth+shell_width, lower_height +shell_width*2))
            bpy.context.object.name = "latch"
            
            bpy.ops.object.modifier_add(type='BOOLEAN')
            bpy.context.object.modifiers["Boolean"].operation = 'UNION'
            bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_attachment"]
            bpy.ops.object.modifier_apply(modifier="Boolean")
            
            bpy.ops.object.modifier_add(type='BOOLEAN')
            bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_cutout"]
            bpy.ops.object.modifier_apply(modifier="Boolean")
            

            bpy.ops.object.select_pattern(pattern="latch_cutout", extend = False)
            bpy.ops.object.select_pattern(pattern="latch_attachment", extend = True)
            bpy.ops.object.delete(use_global=False, confirm=False)
        

    for i in range(-1*(side_latches-1), side_latches, 2):
        bpy.ops.mesh.primitive_cube_add(size=1, 
            enter_editmode=False, 
            align='WORLD', 
            location=(length/2+shell_width+latch_depth/2, i*width/(2*side_latches), 0),
            scale=( latch_depth+shell_width, latch_width-(latch_width-latch_inner_width), lower_height +shell_width*2))
        bpy.context.object.name = "latch_cutout"
        bpy.ops.mesh.primitive_cube_add(size=1, 
            enter_editmode=False, 
            align='WORLD', 
            location=(length/2+shell_width+latch_depth/2, i*width/(2*side_latches), 0),
            scale=( latch_depth+shell_width, latch_width, lower_height +shell_width*2))
        bpy.context.object.name = "latch"
        bpy.ops.object.modifier_add(type='BOOLEAN')

        bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_cutout"]

        bpy.ops.object.modifier_apply(modifier="Boolean")
        bpy.ops.object.select_pattern(pattern="latch_cutout", extend = False)
        
        
        bpy.ops.object.delete(use_global=False, confirm=False)
        
        bpy.ops.mesh.primitive_cube_add(size=1, 
            enter_editmode=False, 
            align='WORLD', 
            location=(-1*(length/2+shell_width+latch_depth/2), i*width/(2*side_latches), 0),
            scale=( latch_depth+shell_width, latch_width-(latch_width-latch_inner_width), lower_height +shell_width*2))
        bpy.context.object.name = "latch_cutout"
        bpy.ops.mesh.primitive_cube_add(size=1, 
            enter_editmode=False, 
            align='WORLD', 
            location=(-1*(length/2+shell_width+latch_depth/2), i*width/(2*side_latches), 0),
            scale=(latch_depth+shell_width, latch_width, lower_height +shell_width*2))
        bpy.context.object.name = "latch"
        bpy.ops.object.modifier_add(type='BOOLEAN')

        bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_cutout"]

        bpy.ops.object.modifier_apply(modifier="Boolean")
        bpy.ops.object.select_pattern(pattern="latch_cutout", extend = False)
        bpy.ops.object.delete(use_global=False, confirm=False)
        


    bpy.ops.object.select_pattern(pattern='latch*')
    bpy.ops.object.join()
    bpy.context.object.name = "latches"


    bpy.ops.mesh.primitive_cylinder_add(radius=1.5, 
        depth=width+shell_width*2, 
        enter_editmode=False, 
        align='WORLD', 
        location=(length/2+shell_width+latch_depth/2, 0, lower_height/2 -6), 
        scale=(1, 1, 1))
    bpy.context.object.name = "latch_hole0"
    bpy.ops.transform.rotate(value=1.5708, 
        orient_axis='X',
        orient_type='GLOBAL', 
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
        orient_matrix_type='GLOBAL', 
        constraint_axis=(True, False, False), 
        mirror=False, 
        use_proportional_edit=False, 
        proportional_edit_falloff='SMOOTH', 
        proportional_size=1, 
        use_proportional_connected=False, 
        use_proportional_projected=False, 
        snap=False, 
        snap_elements={'INCREMENT'}, 
        use_snap_project=False, 
        snap_target='CLOSEST', 
        use_snap_self=True, 
        use_snap_edit=True, 
        use_snap_nonedit=True, 
        use_snap_selectable=False)

    bpy.ops.mesh.primitive_cylinder_add(radius=1.5, 
        depth=width+shell_width*2, 
        enter_editmode=False, 
        align='WORLD', 
        location=(-1*(length/2+shell_width+latch_depth/2), 0, lower_height/2 -6), 
        scale=(1, 1, 1))
    bpy.context.object.name = "latch_hole1"

    bpy.ops.transform.rotate(value=1.5708, 
        orient_axis='Y',
        orient_type='GLOBAL', 
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
        orient_matrix_type='GLOBAL', 
        constraint_axis=(True, False, False), 
        mirror=False, 
        use_proportional_edit=False, 
        proportional_edit_falloff='SMOOTH', 
        proportional_size=1, 
        use_proportional_connected=False, 
        use_proportional_projected=False, 
        snap=False, 
        snap_elements={'INCREMENT'}, 
        use_snap_project=False, 
        snap_target='CLOSEST', 
        use_snap_self=True, 
        use_snap_edit=True, 
        use_snap_nonedit=True, 
        use_snap_selectable=False)

    bpy.ops.mesh.primitive_cylinder_add(radius=1.5, 
        depth=length+shell_width*2, 
        enter_editmode=False, 
        align='WORLD', 
        location=(0, width/2+shell_width+1/3*shell_width+latch_depth/2, lower_height/2 -6), 
        scale=(1, 1, 1))
    bpy.context.object.name = "latch_hole2"

    bpy.ops.transform.rotate(value=1.5708, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False)


    bpy.ops.mesh.primitive_cylinder_add(radius=1.5, 
        depth=length+shell_width*2, 
        enter_editmode=False, 
        align='WORLD', 
        location=(0, -1*(width/2+shell_width+1/3*shell_width+latch_depth/2), lower_height/2 -6), 
        scale=(1, 1, 1))
    bpy.context.object.name = "latch_hole3"


    bpy.ops.transform.rotate(value=1.5708, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False)

    bpy.context.view_layer.objects.active = bpy.data.objects['latches']

    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_hole1"]
    bpy.ops.object.modifier_apply(modifier="Boolean")

    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_hole2"]
    bpy.ops.object.modifier_apply(modifier="Boolean")


    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_hole3"]
    bpy.ops.object.modifier_apply(modifier="Boolean")


    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latch_hole0"]
    bpy.ops.object.modifier_apply(modifier="Boolean")


    bpy.ops.object.select_pattern(pattern="latch_hole*", extend = False)
    bpy.ops.object.delete(use_global=False, confirm=False)



    ##############################################################################################################

    # Outer Shell

    bpy.ops.mesh.primitive_cube_add(size=1, 
        enter_editmode=False, 
        align='WORLD', 
        location=(0, 0, 0),
        scale=(length+shell_width*2, width+shell_width*2, lower_height +shell_width*2))



    ### Outer bevel:
    bpy.ops.object.modifier_add(type='BEVEL')
    bpy.context.object.modifiers["Bevel"].width = bevel_radius
    bpy.context.object.modifiers["Bevel"].segments = 32
    bpy.context.object.modifiers["Bevel"].limit_method = 'WEIGHT'
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='DESELECT')


    obj = bpy.context.edit_object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)
    bm.edges.ensure_lookup_table()
    bm.edges[0].select = True
    bm.edges[1].select = True
    bm.edges[3].select = True
    bm.edges[4].select = True
    bm.edges[6].select = True
    bm.edges[7].select = True
    bm.edges[9].select = True
    bm.edges[10].select = True
    bmesh.update_edit_mesh(me)

    bpy.ops.transform.edge_bevelweight(value=1)

    bpy.ops.object.editmode_toggle()

    bpy.ops.object.modifier_apply(modifier="Bevel")

    bpy.ops.object.editmode_toggle()


    bpy.ops.mesh.select_all(action='DESELECT')

    obj = bpy.context.edit_object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)
    bm.faces.ensure_lookup_table()
    bm.faces[2].select = True
    bmesh.update_edit_mesh(me)
    if top:
        bpy.ops.mesh.inset(thickness=shell_width*2/3/2, depth=0)
        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, -2), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})

        bpy.ops.mesh.inset(thickness=shell_width*2/3/2, depth=0)

        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, 2), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})


        bpy.ops.mesh.inset(thickness=shell_width*1/3, depth=0)

        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, -1*lower_height -shell_width), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})
    else:
        bpy.ops.mesh.inset(thickness=shell_width*0.35, depth=0)
        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, 1), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})

        bpy.ops.mesh.inset(thickness=shell_width*0.3, depth=0)

        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, -1), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})


        bpy.ops.mesh.inset(thickness=shell_width*0.35, depth=0)

        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, 
            TRANSFORM_OT_translate={"value":(0, 0, -1*lower_height -shell_width), 
                "orient_axis_ortho":'X', 
                "orient_type":'NORMAL', 
                "orient_matrix":((0, -1, 0), (1, 0, -3.02263e-09), 
                (3.02263e-09, 0, 1)), 
                "orient_matrix_type":'NORMAL', 
                "constraint_axis":(False, False, True), 
                "mirror":False, 
                "use_proportional_edit":False, 
                "proportional_edit_falloff":'SMOOTH', 
                "proportional_size":1, 
                "use_proportional_connected":False, 
                "use_proportional_projected":False, 
                "snap":False, 
                "snap_elements":{'INCREMENT'}, 
                "use_snap_project":False, "snap_target":'CLOSEST', 
                "use_snap_self":True, 
                "use_snap_edit":True, 
                "use_snap_nonedit":True, 
                "use_snap_selectable":False, 
                "snap_point":(0, 0, 0), 
                "snap_align":False, 
                "snap_normal":(0, 0, 0), 
                "gpencil_strokes":False, 
                "cursor_transform":False, 
                "texture_space":False, 
                "remove_on_cancel":False, 
                "view2d_edge_pan":False, 
                "release_confirm":False, 
                "use_accurate":False, 
                "use_automerge_and_split":False})



    bpy.ops.object.editmode_toggle()



    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].operation = 'UNION'

    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["latches"]

    bpy.ops.object.modifier_apply(modifier="Boolean")



    bpy.ops.object.select_pattern(pattern="latches", extend = False)
    bpy.ops.object.delete(use_global=False, confirm=False)
    if top == False:
        bpy.context.object.name = "box_bottom"
    else:
        bpy.context.object.name = "box_top"

create_box(True, length, width, lower_height, shell_width)

###########################################################################################################
#### Seal ####
##############
bpy.ops.mesh.primitive_cube_add(
    size=1,
    enter_editmode=False, 
    align='WORLD', 
    #location=(0, width+shell_width+25, 0), 
    location = (0, 0, lower_height/2+shell_width-1.15),
    scale=(length+shell_width*2, width+shell_width*2, 2))


bpy.ops.object.modifier_add(type="BOOLEAN")
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["box_top"]
bpy.ops.object.modifier_apply(modifier="Boolean")

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action='SELECT')


bpy.ops.mesh.separate(type='LOOSE')

bpy.ops.object.editmode_toggle()

bpy.context.view_layer.objects.active = bpy.data.objects['Cube.004']
bpy.context.object.name = "seal"
bpy.ops.object.select_pattern(pattern="cube*", extend = False)
bpy.ops.object.delete(use_global=False, confirm=False)



bpy.context.object.location[2] = 0
bpy.context.object.location[1] = width + shell_width + 25

#bpy.ops.object.select_all(action='SELECT')
#bpy.context.object.hide_select = True


create_box(False, length, width, upper_height, shell_width)
bpy.context.object.location[1] = width*2 + shell_width*2 + 50
